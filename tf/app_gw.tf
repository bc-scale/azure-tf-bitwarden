#Log Analytics Workspace
##resource "azurerm_log_analytics_workspace" "wks" {
##  name                = "${var.project_name}-logaw1"
##  location            = azurerm_resource_group.this.location
##  resource_group_name = azurerm_resource_group.this.name
##  sku                 = "PerGB2018" #(Required) Specifies the Sku of the Log Analytics Workspace. Possible values are Free, PerNode, Premium, Standard, Standalone, Unlimited, and PerGB2018 (new Sku as of 2018-04-03).
##  retention_in_days   = 100         #(Optional) The workspace data retention in days. Possible values range between 30 and 730.
##}
##
##resource "azurerm_log_analytics_solution" "agw" {
##  solution_name         = "AzureAppGatewayAnalytics"
##  location              = azurerm_resource_group.this.location
##  resource_group_name   = azurerm_resource_group.this.name
##  workspace_resource_id = azurerm_log_analytics_workspace.wks.id
##  workspace_name        = azurerm_log_analytics_workspace.wks.name
##
##  plan {
##    publisher = "Microsoft"
##    product   = "OMSGallery/AzureAppGatewayAnalytics"
##  }
##}

# Public IP
resource "azurerm_public_ip" "agw" {
  name                = "${var.project_name}-agw-pip1"
  location            = azurerm_resource_group.this.location
  resource_group_name = azurerm_resource_group.this.name
  allocation_method   = "Static"
  sku                 = "Standard"
}

#Application Gateway
resource "azurerm_application_gateway" "agw" {
  depends_on          = [azurerm_key_vault_certificate.this] #, time_sleep.wait_60_seconds]
  name                = "${var.project_name}-agw"
  location            = azurerm_resource_group.this.location
  resource_group_name = azurerm_resource_group.this.name
  enable_http2        = true
  zones               = var.zones

  sku {
    name = var.application_gateway_sku
    tier = var.application_gateway_sku
  }

  autoscale_configuration {
    min_capacity = var.application_gateway_capacity.min
    max_capacity = var.application_gateway_capacity.max
  }

  identity {
    type         = "UserAssigned"
    identity_ids = [azurerm_user_assigned_identity.agw.id]
  }

  gateway_ip_configuration {
    name      = "${var.project_name}-agw-ip-configuration"
    subnet_id = azurerm_subnet.public.id
  }

  frontend_ip_configuration {
    name                 = var.frontend_ip_config_public
    public_ip_address_id = azurerm_public_ip.agw.id
  }

  frontend_port {
    name = var.http.frontend_port
    port = 80
  }

  frontend_port {
    name = var.https.frontend_port
    port = 443
  }

  backend_address_pool {
    name  = var.backend_pool.name
    fqdns = [azurerm_network_interface.private.private_ip_address]
    #fqdns = local.backend_address_pool.fqdns
  }

  ssl_certificate {
    name                = azurerm_key_vault_certificate.this.name
    key_vault_secret_id = azurerm_key_vault_certificate.this.secret_id
  }

  backend_http_settings {
    name                  = var.backend_pool.http_settings
    cookie_based_affinity = "Disabled"
    port                  = 80
    protocol              = "Http"
    host_name             = azurerm_network_interface.private.private_ip_address
    request_timeout       = 1
  }

  # HTTP
  http_listener {
    name                           = var.http.listener
    frontend_ip_configuration_name = var.frontend_ip_config_public
    frontend_port_name             = var.http.frontend_port
    protocol                       = "Http"
  }

  redirect_configuration {
    name                 = var.http.redirect_rule
    redirect_type        = "Permanent"
    include_path         = true
    include_query_string = true
    target_listener_name = var.https.listener
  }

  # http -> https
  request_routing_rule {
    name                        = var.http.route_rule
    rule_type                   = "Basic"
    http_listener_name          = var.http.listener
    redirect_configuration_name = var.http.redirect_rule
  }

  # HTTPS
  http_listener {
    name                           = var.https.listener
    frontend_ip_configuration_name = var.frontend_ip_config_public
    frontend_port_name             = var.https.frontend_port
    protocol                       = "Https"
    ssl_certificate_name           = azurerm_key_vault_certificate.this.name
  }

  request_routing_rule {
    name                       = var.https.route_rule
    rule_type                  = "Basic"
    http_listener_name         = var.https.listener
    backend_address_pool_name  = var.backend_pool.name
    backend_http_settings_name = var.backend_pool.http_settings
  }
}

#Logging
#resource "azurerm_monitor_diagnostic_setting" "agw" {
#  name                       = "${var.project_name}-agw-diag"
#  target_resource_id         = azurerm_application_gateway.agw.id
#  log_analytics_workspace_id = azurerm_log_analytics_workspace.wks.id
#  dynamic "log" {
#    for_each = local.diag_appgw_logs
#    content {
#      category = log.value
#
#      retention_policy {
#        enabled = false
#      }
#    }
#  }
#
#  dynamic "metric" {
#    for_each = local.diag_appgw_metrics
#    content {
#      category = metric.value
#
#      retention_policy {
#        enabled = false
#      }
#    }
#  }
#}
