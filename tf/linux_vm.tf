resource "azurerm_public_ip" "this" {
  name                = "public_ip"
  resource_group_name = azurerm_resource_group.this.name
  location            = azurerm_resource_group.this.location
  allocation_method   = "Dynamic"
}

resource "azurerm_network_interface" "private" {
  name                = "private"
  resource_group_name = azurerm_resource_group.this.name
  location            = azurerm_resource_group.this.location

  ip_configuration {
    name                          = "primary"
    subnet_id                     = azurerm_subnet.private.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = azurerm_public_ip.this.id
  }
}

resource "azurerm_network_security_group" "this" {
  name                = "default"
  location            = azurerm_resource_group.this.location
  resource_group_name = azurerm_resource_group.this.name
}

resource "azurerm_network_security_rule" "https" {
  name                        = "https"
  access                      = "Allow"
  priority                    = 100
  direction                   = "Inbound"
  protocol                    = "Tcp"
  source_port_range           = "*"
  source_address_prefix       = "*"
  destination_port_range      = "443"
  destination_address_prefix  = azurerm_network_interface.private.private_ip_address
  resource_group_name         = azurerm_resource_group.this.name
  network_security_group_name = azurerm_network_security_group.this.name
}

resource "azurerm_network_security_rule" "http" {
  name                        = "http"
  access                      = "Allow"
  priority                    = 102
  direction                   = "Inbound"
  protocol                    = "Tcp"
  source_port_range           = "*"
  source_address_prefix       = "*"
  destination_port_range      = "80"
  destination_address_prefix  = azurerm_network_interface.private.private_ip_address
  resource_group_name         = azurerm_resource_group.this.name
  network_security_group_name = azurerm_network_security_group.this.name
}

resource "azurerm_network_security_rule" "ssh" {
  name                        = "ssh"
  access                      = "Allow"
  priority                    = 101
  direction                   = "Inbound"
  protocol                    = "Tcp"
  source_port_range           = "*"
  source_address_prefix       = var.ssh.public_ip_prefix
  destination_port_range      = "22"
  destination_address_prefix  = azurerm_network_interface.private.private_ip_address
  resource_group_name         = azurerm_resource_group.this.name
  network_security_group_name = azurerm_network_security_group.this.name
}

resource "azurerm_network_interface_security_group_association" "main" {
  network_interface_id      = azurerm_network_interface.private.id
  network_security_group_id = azurerm_network_security_group.this.id
}

#Cloud-init: https://registry.terraform.io/providers/hashicorp/template/latest/docs/data-sources/cloudinit_config
data "template_file" "this" {
  template = file("${path.module}/cloud-init.tpl")

  vars = {
    username       = var.ssh.username
    ssh_public_key = file(var.ssh.public_key)
    docker_assets  = "/mnt/${azurerm_storage_account.this.name}/${var.azure_file_share.name}/${var.azure_file_share.zip_file}"
  }
}

data "template_cloudinit_config" "this" {
  gzip          = true
  base64_encode = true

  part {
    filename     = "init.cfg"
    content_type = "text/cloud-config"
    content      = data.template_file.this.rendered
  }
}

resource "azurerm_linux_virtual_machine" "this" {
  name                = "docker-host"
  resource_group_name = azurerm_resource_group.this.name
  location            = azurerm_resource_group.this.location
  size                = var.vm.size
  admin_username      = var.ssh.username

  custom_data = data.template_cloudinit_config.this.rendered

  network_interface_ids = [
    azurerm_network_interface.private.id
  ]

  admin_ssh_key {
    username   = var.ssh.username
    public_key = file(var.ssh.public_key)
  }

  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }

  source_image_reference {
    publisher = "Canonical"
    offer     = "0001-com-ubuntu-server-focal"
    sku       = "20_04-lts-gen2"
    version   = "latest"
  }

  # Mount the Azure Storage File Share
  provisioner "remote-exec" {
    inline = [
      "sudo mkdir -p /mnt/${azurerm_storage_account.this.name}/${var.azure_file_share.name}",
      "sudo mount -t cifs //${azurerm_storage_account.this.name}.file.core.windows.net/${var.azure_file_share.name} /mnt/${azurerm_storage_account.this.name}/${var.azure_file_share.name} -o vers=3.0,dir_mode=0755,file_mode=0755,serverino,username=${azurerm_storage_account.this.name},password=${azurerm_storage_account.this.primary_access_key}",
    ]

    connection {
      type        = "ssh"
      user        = var.ssh.username
      private_key = file(var.ssh.private_key)
      host        = azurerm_linux_virtual_machine.this.public_ip_address
    }
  }
}

output "vm_public_ip" {
  value = azurerm_linux_virtual_machine.this.public_ip_addresses
}